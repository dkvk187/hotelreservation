package com.mobi.hotelbooking.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.mobi.hotelbooking.BookingDetails;
import com.mobi.hotelbooking.repo.BookingDetailsRepo;

public class BookingDetailsService {

	@Autowired
	BookingDetailsRepo bookingDetailsRepo;

	public void insertBookingDetails(BookingDetails bookingDetails) {
		bookingDetailsRepo.insertBookingDetails(bookingDetails);
	}

	public int fetchBookingDetails(BookingDetails bookingDetails) {
		int result = 0;
		result = bookingDetailsRepo.fetchBookingDetails(bookingDetails);
		return result;

	}

}
