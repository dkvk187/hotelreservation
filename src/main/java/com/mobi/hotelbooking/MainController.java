package com.mobi.hotelbooking;

import java.time.LocalDateTime;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.mobi.hotelbooking.BookingDetails;
@Controller
public class MainController {
	
	@GetMapping("/register")
	public String showForm(Model model) {
		BookingDetails bookingDetails = new BookingDetails();
		model.addAttribute("bookingDetails", bookingDetails);
		
		/*
		 * List<String> listProfession = Arrays.asList("Developer", "Tester",
		 * "Architect"); model.addAttribute("listProfession", listProfession);
		 */
		return "register_form";
	}
	
	@PostMapping("/register")
	public String submitForm(@ModelAttribute("user") User user) {
		System.out.println(user);
		return "register_success";
	}
	
	@PostMapping("/confirmation")
	public String bookRoom(@ModelAttribute("bookingDetails") BookingDetails bookingDetails) {

		
		//model.addAttribute("bookingDetails", bookingDetails);
		return "register_success";
	}
}
