package com.mobi.hotelbooking.repo;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.mobi.hotelbooking.BookingDetails;

@Repository
public class BookingDetailsRepoImpl implements BookingDetailsRepo {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insertBookingDetails(BookingDetails bookingDetails) {
		String sql = "INSERT INTO mobiHotelBooking.BOOKING_DETAILS('mobileNumber','bookingName','checkInDate','checkOutdate','numberOfRooms') VALUES (?,?,?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int result = 0;
		try {
			result = jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, bookingDetails.getMobileNumber());
				ps.setString(2, bookingDetails.getName());
				Instant inst = Instant.parse(bookingDetails.getCheckInDate());
				ps.setTimestamp(3, Timestamp.from(inst));
				Instant instCheckOut = Instant.parse(bookingDetails.getCheckOutDate());
				ps.setTimestamp(4, Timestamp.from(instCheckOut));
				ps.setString(5, bookingDetails.getNumberOfRooms());
				return ps;
			}, keyHolder);

		} catch (Exception e) {

		}
		return result;
	}

	@Override
	public int updateBookingDetails(BookingDetails bookingDetails) {

		return 0;
	}

	@Override
	public int fetchBookingDetails(BookingDetails bookingDetails) {
		int result = 0;
		String sql = "SELECT COUNT(*) FROM mobiHotelBooking.BOOKING_DETAILS WHERE checkInDate = ?";
		
		result=jdbcTemplate.queryForObject(sql, new Object[] {bookingDetails.getCheckInDate()}, Integer.class);
		return result;
	}

}
